const {checkAge} = require('./../src/util.js')
const {checkName} = require('./../src/util.js')
const {assert} = require('chai');


describe('test_checkAge', () => {
	it('age_not_empty', () => {
		const age = "";
		assert.isNotEmpty(checkAge(age));
	});

	it('age_not_undefined', () => {
		const age = undefined;
		assert.isDefined(checkAge(age));
	})

	it('age_not_null', () => {
		const age = null;
		assert.isNotNull(checkAge(age));
	})

	it('age_is_string', () => {
		const age = '';
		assert.isString(checkAge(age))
	})

	it('age_is_not_zero', () => {
		const age = 0;
		assert.isNotEmpty(checkAge(age))  //wrong
	})
})

describe('test_checkName', () => {
	it('name_not_empty', () => {
		const name = "";
		assert.isNotEmpty(checkName(name));
	});

	it('name_not_undefined', () => {
		const name = undefined;
		assert.isDefined(checkName(name));
	})

	it('name_not_null', () => {
		const name = null;
		assert.isNotNull(checkName(name));
	})

	it('name_is_string', () => {
		const name = '';
		assert.isString(checkName(name))
	})

	it('name_is_not_zero', () => {
		const name = '';
		assert.isNotEmpty(checkName(name))  //wrong
	})
})