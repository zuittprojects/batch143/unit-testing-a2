function checkAge(age){
	// To resolve the failed test from our unit testing, we will create a solution
	if(age == ""){
		return "Error: Age is Empty";
	}
	

	if(age == undefined){
		return "Error: Age is undefined"
	}

	if(age == null){
		return "Error: Age is null"
	}

	if(age == 0){
		return "Error: Age Must Be More Than Zero(0)"
	}

	return age;
}

function checkName(name){
	// To resolve the failed test from our unit testing, we will create a solution
	if(name == ""){
		return "Error: Name is Empty";
	}
	

	if(name == undefined){
		return "Error: Name is undefined"
	}

	if(name == null){
		return "Error: Name is null"
	}

	if(typeof(name) !== 'string'){
		return "Error: Name not a string"
	}

	if((name) == ""){
		return "Error: Name must be filled up"
	}
	return name;
}


module.exports = {
	checkAge,
	checkName
}